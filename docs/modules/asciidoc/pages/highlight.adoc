= Highlight
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
:example-caption!:
:hash: #
// URLs
:url-adoc-manual: https://asciidoctor.org/docs/user-manual
:url-highlight: {url-adoc-manual}/#custom-styling-with-attributes

On this page, you'll learn:

* [x] How to highlight text with AsciiDoc.

== Highlight syntax

To apply highlighting to a word or phrase, place a hash (`{hash}`) at the beginning and end of the text you wish to format.
To highlight one or more characters bounded by other characters, place two hashes (`{hash}{hash}`) before and after the characters.

.Highlight inline formatting
[source,asciidoc]
----
Let's #highlight this phrase# and the i and the s in th##is##.
----

.Result
====
Let's #highlight this phrase# and the i and the s in th##is##.
====

[discrete]
==== Asciidoctor resources

* {url-highlight}[Highlighted text formatting^]
